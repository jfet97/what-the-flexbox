# FLEXBOX

&nbsp;

## Creazione del flex container

```css
.container{
    display: flex;
}
```

Il container occuperà tutta la larghezza della pagina, gli item diventeranno automaticamente flex item e si disporranno in orrizzontale.

Alternativa:

```css
.container{
     display: inline-flex;
}
```

Il container occuperà in larghezza solo lo spazio necessario.

&nbsp;

## Flex Direction

Vi sono due assi: il **main** axis e il **cross** axis. Gli elementi si dispongono sempre a partire dall'origine del main axis, lungo esso.

&nbsp;

```css
.container{
     display: flex;
     flex-direction: row;
}
```

È il valore di default. Gli elementi si disporranno, allineati, da sinistra verso destra.

Main axis: *SX* -> *DX*

Cross axis: *UP* -> *BOTTOM*

&nbsp;

```css
.container{ 
    display: flex; 
    flex-direction: row-reverse;
}
```

Gli elementi si disporranno da destra verso sinistra.

Main axis: *DX* -> *SX*

Cross axis: *UP* -> *BOTTOM*
&nbsp;

```css
.container{ 
    display: flex; 
    flex-direction: column;
}
```

Gli elementi si disporranno dall'alto verso il basso.

Main axis: *UP* -> *BOTTOM*

Cross axis: *SX* -> *DX*
&nbsp;

```css
.container{ 
    display: flex; 
    flex-direction: column-reverse;
}
```

Gli elementi si disporranno dal besso verso l'alto.

Main axis: *BOTTOM* -> *UP*

Cross axis: *SX* -> *DX*

&nbsp;

## Flex Wrap

Impostiamo il comportamento da seguire nel caso in cui ci sia bisogno di "andare a capo" per mantenere la larghezza dei flex item. 

Gli item "andranno a capo" seguendo il verso del cross axis.

&nbsp;

```css
.container{
     display: flex;
     flex-wrap: nowrap;
}
```

Di default si impedisce che gli item "vadano a capo", in automatico la larghezza di ogni singolo item viene ristretta per non dover attivare la scrolling orrizzontale. Quando la larghezza, in fase di decrescita, diventa minore della larghezza del **contenuto** del singolo item, per non far andare in overflow gli elementi presenti all'interno, viene attivato lo scroll orrizzontale.

&nbsp;

```css
.container{
     display: flex;
     flex-wrap: wrap;
}
```

Questa impostazione fa si che la larghezza degli item debba essere rispettata, mai rimpicciolita, quindi essi "vanno a capo".

&nbsp;

Nel caso in cui la `flex-direction` fosse stata impostata su `column`, se il flex container possiede un'altezza specificata, allora si "andrà a capo" in una maniera particolare: verrà formata una nuova colonna a destra.

&nbsp;

```css
.container{
     display: flex;
     flex-wrap: wrap-reverse;
}
```

Il valore `wrap-reverse` permette di invertire il verso del cross axis.

&nbsp;

## Flex Flow

Combina `flex-direction` e `flex-wrap` insieme, dato che spesso necessitiamo di impostarle entrambe.

&nbsp;

```css
.container{
     display: flex;
     flex-direction: column;
     flex-wrap: wrap;
}
```

Può essere riassunto con:

```css
.container{
     display: flex;
     flex-flow: column wrap;
}
```

&nbsp;

## Order

Di default tutti gli item hanno order pari a 0 e quindi verranno inseriti all'interno del container seguendo esattamente l'ordine di comparizione nel documento `html`. Possiamo però modificare questo order, in modo da modificare la precedenza di uno o più item.

&nbsp;

```css
item-1{
    order: 1;
}
```

Poiché tutti gli item hanno `order:0`, `item-1` sarà l'ultimo.

&nbsp;

```css
item-2{
    order: -1;
}
```

Poiché tutti gli item hanno `order:0`, `item-2` sarà il primo.

&nbsp;

## Justify Content

Modifica l'allineamento del contenuto del container <u>rispetto al main axis</u>.

&nbsp;

- `justify-content: flex-start`: default, gli item si disporranno all'inizio del container; **eventuale** spazio in più andrà alla fine

- `justify-content: flex-end`: gli item si disporranno alla fine del container; **eventuale** spazio in più andrà all'inizio

- `justify-content:center`: gli item si disporranno al centro del container; **eventuale** spazio in più si distribuirà equamente a destra e a sinistra

- `justify-content: space-between`:  il primo item si disporrà all'inizio senza lasciare spazio, l'ultimo si disporrà alla fine senza lasciare spazio, mentre gli altri si distribuiranno tra essi con l'**eventuale** spazio in più che si suddividerà equamente in `numero(items) -1` parti per creare spazio tra gli elementi

- `justify-content: space-around`: l'**eventuale** spazio in più si suddividerà equamente in `numero(items)` parti, ognuna delle quali si suddividerà a sua volta a metà dove ogni metà andrà una prima e l'altra dopo ogni item per creare spazio tra essi

&nbsp;

Se `flex-direction: column`, dove il main axis va dall'alto al basso, dobbiamo impostare un'altezza al container per vedere degli effetti, altrimenti sicuramente non ci sarebbe alcun spazio in più.

Impostando un'altezza, `flex-direction:column` e `justify-content:center` abbiamo centrato verticalmente il contenuto. 

&nbsp;

![flex-pack.svg](C:\Users\39329\Downloads\flex-pack.svg)

&nbsp;

## Align items

Modifica l'allineamento del contenuto del container, <u>rispetto al cross axis</u>, **quando vi è una sola riga/colonna di item**.

&nbsp;

- `align-items: stretch`: default, gli item prenderanno tutta l'altezza disponibile, se `flex-direction: row`, altrimenti tutta la larghezza disponibile se `flex-direction: column`

- `align-items: center`: come justify content

- `align-items: flex-start`: come justify content

- `align-items: flex-end`: come justify content

- `align-items: baseline`: gli item verranno allineati in base alla **baseline** del loro contenuto
  
  &nbsp;

![flex-align.svg](C:\Users\39329\Downloads\flex-align.svg)

&nbsp;

## Align Content

Modifica l'allineamento del contenuto del container, <u>rispetto al cross axis</u>, **quando si creano più righe/colonne di item**.

&nbsp;

- `align-content: stretch`: default, gli item prenderanno tutta l'altezza/larghezza disponibile
- `align-content: flex-start`: come justify content
- `align-content: flex-end`: come justify content
- `align-content: center`: come justify content
- `align-content: space-between`: come justify content
- `align-content: space-around`: come justify content

&nbsp;

![align-content-example.svg](C:\Users\39329\Downloads\align-content-example.svg)

&nbsp;

## Align Items & Align Content

Nel caso in cui vi sia una sola riga/colonna, `align-items` gestisce l'allineamento lungo il cross axis degli elementi che sono disposti sul main axis e `align-content` **non ha alcun effetto**.

Nel caso in cui vengano generate più righe/colonne di item in realtà vengono generati più main axis, uno per ogni riga/colonna. La proprietà `align-items` gestisce l'allineamento lungo il cross axis **per ogni** main axis, a meno che `align-content` non sia impostato diversamente da `stretch`.

Questo significa che non siamo obbligati ad usare `align-content` per gestire l'allineamento in caso di `wrap`. Ovviamente `align-items` e `align-content` non possono essere utilizzati interscambiabilmente: semplicemente possiamo ottenere effetti diversi, particolari, in caso di necessità. Il primo appllicherà la medesima regola su ogni main axis come se ognuno di esso fosse l'unico, ovviamente in uno spazio più ristretto. Il secondo invece agisce sui vari main axis in quanto insieme.

&nbsp;

P.S. Una proprietà come `justify-items` non esiste dato che non è mai possibile ottenere più cross axis, `wrap` o `no-wrap` che sia.

&nbsp;

## Align Self

Ci permette di sovrascrivere il valore impostato con `align-items` per uno specifico item.

```css
item-1{
    align-self: center;
}
```

I valori della proprietà sono esattamente quelli di `align-items`.

&nbsp;

## Flex

Anche questa proprietà aiuta a gestire l'eventuale spazio in più, modificando però le dimensioni degli item. La proprietà deve essere impostata perciò sugli item e non sul container.

&nbsp;

```css
item-1{
    flex: 0 1 auto;
}
```

Il valore di default è `0 1 auto`.

&nbsp;

Solitamente ogni item riceve `flex: 1`, in modo che abbia la medesima grandezza inipendentemente dal contenuto. Potremmo comunque scegliere di impostare un item con `flex:2`: esso sarà grande il doppio rispetto agli altri item.

Oppure potremmo impostare `flex: n` solo ad alcuni item. Così facendo solo ad essi verrà ridistribuito proporzionalmente (`n`) lo spazio in più.

Ovviamente in caso di spazio mancante, senza la possibilità di wrapping, gli item dovranno rimpicciolirsi e le proporzioni si perderanno.

&nbsp;

### flex: grow shrink basis;

La proprietà `flex` riassume in realtà le proprietà `flex-grow`, `flex-shrink` e `flex-basis`, nell'ordine evidenziato nel sottotitolo.

&nbsp;

#### flex-grow

Specifica cosa fare dell'eventuale spazio libero in eccesso, in quale quantità l'item lo assorbirà.

&nbsp;

```css
item-1{
    flex-grow: 0; /* default */
}
```

L'item non prenderà nemmeno un briciolo di un eventuale spazio in più.

&nbsp;

```css
item-1{
    flex-grow: 1;
}
```

L'item prenderà una parte dell'eventuale spazio in più con fattore di proporzionalità **1**.

&nbsp;

#### flex-shrink

Specifica cosa fare in caso di spazio mancante.

&nbsp;

```css
item-1{
    flex-shrink: 0;
}
```

L'item non è disponibile a rimpicciolirsi. In caso di impossibilità di wrapping si ricade o nello scrolling o nell'overflow.

&nbsp;

```css
item-1{
    flex-shrink: 1; /* default */
}
```

L'item è disposto a rimpicciolirsi con fattore di proporzionalità **1**. Il limite è il contenuto dell'item.

&nbsp;

#### flex-basis

Valore ideale della grandezza degli item. In caso di `flex-direction: row` ci si riferisce alla `width`, in caso di `flex-direction: column` alla `height`.

Ideale perché `flex-grow` e `flex-shrink` hanno una parte fondamentale nel definire la grandezza in caso di spazio in più/ in meno.

&nbsp;

Il valore di default è `auto`, ergo l'eventuale valore di `width`/`height` preimpostato o il contenuto dell'item in caso contrario. Se `flex-basis` è minore della grandezza del contenuto dell'item, esso prenderà comunque lo spazio **minimo** per il proprio contenuto.

&nbsp;

## Valori comuni per flex

- `flex: 0 auto`: è esattamente uguale a `flex: initial`, oltre ad essere lo shorthand per il **valore di default**, cioè `flex: 0 1 auto`. In questo caso le dimensioni dell'item rispetteranno eventuali `width` & `height` preimpostate, o prenderanno il contenuto dell'item come riferimento in caso contrario.
  
  Gli item saranno inflessibili in caso di spazio libero in più, ma permette lo shrink estremo in caso di spazio mancante.

- `flex: auto`: è esattamente uguale a `flex: 1 1 auto` e non è il valore di default. Beware, this is not the default value. In questo caso le dimensioni dell'item rispetteranno eventuali `width` & `height` preimpostate, ma permette ad essi di assorbire l'eventuale spazio libero in più lungo il main axis. 
  
  Se tutti gli item hanno impostato `flex` solo a **uno o più** tra `auto`, `initial` e `none`, l'eventuale spazio libero in più verrà distribuito equamente tra gli item aventi proprio `flex: auto`.

- `flex: none`: è esattamente uguale a `flex: 0 0 auto`. In questo caso le dimensioni dell'item rispetteranno eventuali `width` & `height` preimpostate, ma saranno completamente inflessibili sia in caso di spazio libero in più, sia in caso di spazio mancante. In caso non si possa "andare a capo" si potrebbe ottenere owerflow.

- `flex: n`: è esattamente uguale a `flex: n 1 0px`. Gli item saranno flessibili e avranno `flex-basis` impostata a zero. Se `flex-basis` è minore della grandezza del contenuto dell'item, esso prenderà comunque lo spazio **minimo** per il proprio contenuto. Poiché zero è minore di qualsiasi possibile grandezza, l'item riceverà esattamente la proporzione desiderata (`n`) di eventuale spazio libero in più. 
  
  Se tutti gli item usano questo pattern, la loro grandezza sarà sempre proporzionale al flex factor specificato, cioè `n`.

&nbsp;

## Nozioni utili

- Gli elementi aventi `display: inline` potrebbero creare qualche strano problemino grafico se allineate con flexbox. In caso, ricorrere a `display: block`.

- Il modo di gestire lo spazio di un singolo elemento impostato con `flex-grow` ha maggior priorità rispetto a proprietà come `justify-content`. In caso ricorrere a `flex-grow : 0`.

- Se `flex-basis` è minore della grandezza del contenuto dell'item, esso prenderà comunque lo spazio **minimo** per il proprio contenuto.

- Quando `flex-basis` è `0`/`0px`, le dimensioni preimpostate, cioè eventuale `width`/`height` dell'item, verranno ignorate. Poiché zero è minore di qualsiasi possibile grandezza (vedasi punto precedente), l'item prenderà inizialmente lo spazio strettamente necessario per il proprio contenuto. Eventuale spazio libero verrà ridistribuito a tutti gli item equamente a seconda del `flex-grow` scelto.
  
  Se tutti gli item usano questo pattern, la loro grandezza sarà sempre proporzionale al `flex-grow` scelto. Si parla quindi di **absolute sizing**: a parità di `flex-grow` **sembrerà** che gli item più grossi ottengano meno spazio libero, in proporzione, rispetto agli item più piccoli.

- Quando `flex-basis` è `auto`, viene tenuto conto dello spazio già occupato dall'elemento, compresa un'eventuale grandezza maggiore del contenuto che viene rispettata. Eventuale spazio libero in più verrà ridistributo agli elementi, i quali cresceranno in proprorzione al loro contenuto.
  
  Si parla quindi di **relative sizing**: a parità di `flex-grow` **sembrerà** che gli item più grossi ottengano più spazio libero rispetto agli item più piccoli, per mantenere le proporzioni in base al contenuto o alla grandezza preimpostata.

- Per quanto riguarda l'absolute e il relative sizing, consiglio di dare un'occhiata alla riposta avente più like [a questa domanda](https://stackoverflow.com/questions/43520932/what-is-the-difference-between-flex-basis-auto-and-flex-basis-0/43529572#43529572).

- Per quanto riguarda ciò che avviene in caso di spazio mancante: *the `flex-shrink` factor is multiplied by the flex base size when distributing negative space. This distributes negative space in proportion to how much the item is able to shrink, so that e.g. a small item won’t shrink to zero before a larger item has been noticeably reduced*.

- Nel caso in cui le dimensioni degli item vadano diminuite e il sistema debba scegliere tra il wrapping e lo shrink **sotto** al valore impostato con `flex-basis`, se ha la possibilità di eseguire il wrapping (`flex-wrap: wrap`) allora darà priorità a questa operazione, altrimenti eseguirà lo shrink.
  
  &nbsp;

&nbsp;
